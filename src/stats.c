#include "ft_ping.h"
#include <sys/time.h>
#include <stdio.h>

extern t_sys *sys;

void print_statistics(void)
{
	struct timeval time;
	gettimeofday(&time, NULL);
	long end_time = time.tv_sec * 1000000 + time.tv_usec;
	long total_time = end_time - sys->stats_start_time;
	printf("\n");
	printf("--- %s ping statistics ---\n", sys->addr);
	printf("%d packets transmitted, %d received,",
		   sys->stats_sent_count, sys->stats_recv_count);
	if (sys->stats_err_count)
		printf(" %d errors,", sys->stats_err_count);
	if (sys->stats_sent_count) // prevent division by zero
	{
		printf(" %d%% packet loss, time %ldms\n",
		   (1 - (sys->stats_recv_count / sys->stats_sent_count)) * 100,
			   total_time / 1000l);
	}
	if (sys->stats_recv_count > 0)
	{
		printf("rtt min/avg/max/mdev = %ld.%03ld/%ld.%03ld/%ld.%03ld",
			   sys->stats_rtt_min/1000l, sys->stats_rtt_min%1000l,
			   sys->stats_rtt_avg/1000l, sys->stats_rtt_avg%1000l,
			   sys->stats_rtt_max/1000l, sys->stats_rtt_max%1000l
			);
		if (sys->stats_rtt_mdev/1000l)
			printf("/%ld.%03ld ms\n", sys->stats_rtt_mdev/1000l, sys->stats_rtt_mdev%1000l);
		else
			printf("/(not available) ms\n");
	}
}

void update_statistics(void)
{
	if (sys->stats_rtt_min == 0)
		sys->stats_rtt_min = sys->stats_rtt;
	else
		sys->stats_rtt_min = (sys->stats_rtt < sys->stats_rtt_min) ? sys->stats_rtt : sys->stats_rtt_min;
	if (sys->stats_rtt_max == 0)
		sys->stats_rtt_max = sys->stats_rtt;
	else
		sys->stats_rtt_max = (sys->stats_rtt > sys->stats_rtt_max) ? sys->stats_rtt : sys->stats_rtt_max;
	sys->stats_total_rtt += sys->stats_rtt;
	if (sys->stats_recv_count)
		sys->stats_rtt_avg = sys->stats_total_rtt / sys->stats_recv_count;
	sys->stats_rtt_pow2total += sys->stats_rtt * sys->stats_rtt;
}
