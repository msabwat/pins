#include "ft_ping.h"
#include "libft.h"
#include <stdio.h>

int set_options(t_sys *sys, int ac, char **av)
{
	if (ac == 1)
		return (0);
	else if (ac == 2) {
		if (ft_strcmp(av[1], "-h") == 0) {
			return (0);
		}
		else {
			sys->addr = av[1];
		}
	}
	else
	{
		int opt = ac;
		int i = 1;
		while (i < opt - 1)
		{
			if (ft_strcmp(av[i], "-t") == 0) {
				if (i + 1 == opt - 1)
					return (-1);
				if ((ft_atoi(av[i + 1]) > 0) && (ft_atoi(av[i + 1]) <= 255))
					sys->opts_ttl = ft_atoi(av[i + 1]);
				else
					return (-1);
				i += 2;
				sys->addr = av[opt - 1];
			}
			else if (ft_strcmp(av[i], "-v") == 0) {
				sys->opts_verbose = 1;
				i += 1;
				sys->addr = av[opt - 1];
			}
			else
				return (-1);
		}
	}
	return (1);
}
