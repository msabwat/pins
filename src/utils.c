#include <arpa/inet.h>
#include <netdb.h>
#include <string.h>
#include <stdio.h>

#include "ft_ping.h"
#include "libft.h"

int	resolve_addr(t_sys *sys, const char *addr)
{
	struct addrinfo hints;
	struct addrinfo *result;

	char *new = malloc(sizeof(char) * 100);
	if (!new)
		return (0);
	ft_memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_RAW;
	hints.ai_flags = AI_CANONNAME;
	hints.ai_protocol = 0;
	hints.ai_canonname = NULL;
	hints.ai_addr = NULL;
	hints.ai_next = NULL;
	
	int ret = getaddrinfo(addr, NULL, &hints, &result);
	if (ret != 0) {
		free(new);
		return (0);
	}
	// to do: what if address is valid but not AF_INET (should be able to do ->next until you find the right one before you bail out)
	inet_ntop (result->ai_family, &((struct sockaddr_in *)result->ai_addr)->sin_addr, new, 100);
	sys->dest_addr = result->ai_addr;
	sys->ip_addr = new;

	freeaddrinfo(result);
	return (1);
}
	 

double ft_sqrt(double nb)
{
	double lo;
	double hi;
	double sqrt;
	double acc = 0.0001;

	if (nb < 0)
		return (-1);
	if (nb == 0)
		return (0);
	if (nb < 1) {
		hi = 1;
		lo = nb;
	}
	else {
		lo = 1;
		hi = nb;
	}
	while ((hi - lo) > acc) {
		sqrt = (lo + hi) / 2;
		if (sqrt * sqrt > nb)
			hi = sqrt;
		else
			lo = sqrt;
	}
	return ((lo + hi) / 2);
}
