#include "ft_ping.h"
#include "libft.h"

#include <netinet/ip_icmp.h>
#include <stdlib.h>
#include <stdio.h>
#include <strings.h>
#include <unistd.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>

ssize_t send_echo_packet(t_sys *sys, int sockfd, size_t size, int seq)
{
	struct icmphdr *header;
	int packlen = size + sizeof(struct icmphdr);
	header = malloc(packlen);
	ft_bzero(header, packlen);
	header->type = ICMP_ECHO;
	header->un.echo.id = htons(getpid());
	header->un.echo.sequence = seq;
	header->checksum = 0;
	// RFC 1071
	unsigned short *buf = (unsigned short *)header;
	int len = packlen;
    unsigned int sum = 0;

    for ( sum = 0; len > 1; len -= 2 )
        sum += *buf++;
    if ( len == 1 )
        sum += *(unsigned char*)buf;
    sum = (sum >> 16) + (sum & 0xFFFF);
    sum += (sum >> 16);
	header->checksum = ~sum;
	struct sockaddr_in target_addr;
	memset (&target_addr, 0, sizeof(target_addr));
	target_addr.sin_family = AF_INET;
	target_addr.sin_port = 1;
	target_addr.sin_addr.s_addr = 0;
	if (inet_pton (AF_INET, sys->ip_addr, &target_addr.sin_addr) == -1) {
		printf("ping: error: could not create sockadd_in struct\n");
		if (sys->ip_addr)
			free((void *)(sys->ip_addr));
		free(header);
		free(sys);
		exit(1);
	}
	ssize_t bytes = sendto(sockfd, header, packlen, 0,
						   (struct sockaddr*) &target_addr, sizeof(target_addr));
	if (bytes == -1) {
		printf("ping: error: could not send icmp paquet\n");
		if (sys->ip_addr)
			free((void *)(sys->ip_addr));
		free(header);
		free(sys);
		exit(1);
	}
	if (sys->opts_verbose == 1) {
		printf("----sent icmp header----\n");
		printf("Type : ");
		if (header->type == 0)
			printf("ICMP_ECHOREPLY ");
		else if (header->type == 3)
			printf("ICMP_DEST_UNREACH ");
		else if (header->type == 4)
			printf("ICMP_SOURCE_QUENCH ");
		else if (header->type == 5)
			printf("ICMP_REDIRECT ");
		else if (header->type == 8)
			printf("ICMP_ECHO ");
		else if (header->type == 11)
			printf("ICMP_TIME_EXCEEDED ");
		else if (header->type == 12)
			printf("ICMP_PARAMETERPROB ");
		else if (header->type == 13)
			printf("ICMP_TIMESTAMP ");
		else if (header->type == 14)
			printf("ICMP_TIMESTAMPREPLY ");
		else if (header->type == 15)
			printf("ICMP_INFO_REQUEST ");
		else if (header->type == 16)
			printf("ICMP_INFO_REPLY ");
		else if (header->type == 17)
			printf("ICMP_ADDRESS ");
		else if (header->type == 18)
			printf("ICMP_ADDRESSREPLY ");
		printf("Code: %hhx Id: 0x%x Sequence: %d\n",
			   header->code,
			   header->un.echo.id,
			   header->un.echo.sequence
			);
		printf("------------------------\n");
	}
	free(header);
	return (bytes);
}

void receive_icmp_packet(t_sys *sys, int sockfd, ssize_t packlen)
{
	unsigned char packet[packlen];
	struct msghdr msg;
	char addrbuf[128];
    struct iovec iov;
	// not needed for now: we don't read control messages
	uint8_t ctrlbuffer[CMSG_SPACE(sizeof(uint8_t))];
	
    iov.iov_base = packet;
    iov.iov_len = packlen;

    ft_memset(&msg, 0, sizeof(msg));
    msg.msg_name = addrbuf;
    msg.msg_namelen = sizeof(addrbuf);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;
	msg.msg_control = ctrlbuffer;
	msg.msg_controllen = sizeof(ctrlbuffer);

    int ret = recvmsg(sockfd, &msg, 0);
	if (ret == -1) {
		// bailout
		return ;
	}
		
	void *buf = msg.msg_iov->iov_base;
	struct iphdr *ip = buf;
	struct icmphdr *icmp_reply = buf + sizeof(struct iphdr);
	struct timeval time;
	gettimeofday(&time, NULL);
	long cur = (time.tv_sec * 1000000 + time.tv_usec) - sys->stats_packet_time;
	if (sys->stats_recv_count <= 1)
		sys->stats_rtt_ewma = sys->stats_rtt;
	else
		sys->stats_rtt_ewma = (sys->stats_rtt * 0.1) + (sys->stats_rtt_ewma * 0.9);
	if (icmp_reply->type == ICMP_ECHOREPLY && icmp_reply->un.echo.id == ntohs(getpid() & 0xFFFF)) {
		sys->stats_recv_count += 1;
		sys->stats_rtt = (time.tv_sec * 1000000 + time.tv_usec) - sys->stats_packet_time;
		printf("%zu bytes from %s: icmp_seq=%d ttl=%d time=%ld.%01ld ms\n",
			   (uint16_t)ntohs(ip->tot_len) - sizeof(struct iphdr),
			   inet_ntoa((struct in_addr){.s_addr = ip->saddr}),
			   icmp_reply->un.echo.sequence, ip->ttl, cur/1000l, (cur/1000l ? cur%10l:cur%1000l));
	}
	else {
		// we need to extract the response, in the sent payload
		struct icmphdr *icmp_packet = buf + sizeof(struct iphdr) +
			sizeof(struct icmphdr) + sizeof(struct iphdr);
		if (icmp_packet->un.echo.id == ntohs(getpid() & 0xFFFF)) {
			printf("From %s: ",inet_ntoa((struct in_addr){.s_addr = ip->saddr}));
			printf("icmp_seq=%d ", icmp_packet->un.echo.sequence);
			if (icmp_reply->type == ICMP_TIME_EXCEEDED)
				printf("Time to live exceeded\n");
			else if (icmp_reply->type == 3)
				printf("Destination Unreachable\n");
			else if (icmp_reply->type == 4)
				printf("Source Quench\n");
			else if (icmp_reply->type == 5)
				printf("Redirect (change route)\n");
			else if (icmp_reply->type == 12)
				printf("Parameter Problem\n");
			else if (icmp_reply->type == 13)
				printf("Timestamp Request\n");
			else if (icmp_reply->type == 14)
				printf("Timestamp Reply\n");
			else if (icmp_reply->type == 15)
				printf("Information Request\n");
			else if (icmp_reply->type == 16)
				printf("Information Reply\n");
			else if (icmp_reply->type == 17)
				printf("Address Mask Request\n");
			else if (icmp_reply->type == 18)
				printf("Address Mask Reply\n");
			else
				printf("unknow message type / origin\n");
			sys->stats_err_count += 1;
		}
		else {
			// bail out
			return ;
		}
	}

	if (sys->opts_verbose == 1) {
		printf("----received icmp header----\n");
		printf("Type : ");
		if (icmp_reply->type == 0)
			printf("ICMP_ECHOREPLY ");
		else if (icmp_reply->type == 3)
			printf("ICMP_DEST_UNREACH ");
		else if (icmp_reply->type == 4)
			printf("ICMP_SOURCE_QUENCH ");
		else if (icmp_reply->type == 5)
			printf("ICMP_REDIRECT ");
		else if (icmp_reply->type == 8)
			printf("ICMP_ECHO ");
		else if (icmp_reply->type == 11)
			printf("ICMP_TIME_EXCEEDED ");
		else if (icmp_reply->type == 12)
			printf("ICMP_PARAMETERPROB ");
		else if (icmp_reply->type == 13)
			printf("ICMP_TIMESTAMP ");
		else if (icmp_reply->type == 14)
			printf("ICMP_TIMESTAMPREPLY ");
		else if (icmp_reply->type == 15)
			printf("ICMP_INFO_REQUEST ");
		else if (icmp_reply->type == 16)
			printf("ICMP_INFO_REPLY ");
		else if (icmp_reply->type == 17)
			printf("ICMP_ADDRESS ");
		else if (icmp_reply->type == 18)
			printf("ICMP_ADDRESSREPLY ");
		printf("Code: %hhx Id: 0x%x Sequence: %d\n",
			   icmp_reply->code,
			   icmp_reply->un.echo.id,
			   icmp_reply->un.echo.sequence
			);
		printf("----------------------------\n");
	}
}
