#include "ft_ping.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/ip_icmp.h>
#include <unistd.h>
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>

t_sys *sys;

int init(int ac, char **av)
{
	sys = (t_sys *)malloc(sizeof(t_sys));
	if (!sys)
		goto error;

	sys->ip_addr = NULL;
	
	// init options - default values, can be overwritten by set_options()
	sys->opts_ttl = 64;
	// TODO: option to limit packets sent
	sys->opts_packet_count = 1;
	sys->opts_packet_size = 56;
	sys->opts_verbose = 0;

	// set options
	int ret = set_options(sys, ac, av);
	if (ret == 0) {
		printf("Usage: ft_ping [-v|-t <1-255>] <ip address | dns name>\n");
		printf("Run as root to be able to use raw sockets\n");
		goto error;
	}
	else if (ret == -1) {
		printf("ping: usage error: wrong options (-h to list supported options) \n");
		goto error;
	}

	// create socket
	sys->sockfd = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sys->sockfd == -1) {
		printf("ping: EACCESS The process does not have appropriate privileges\n");
		goto error;
	}	
	if(setsockopt(sys->sockfd, IPPROTO_IP, IP_TTL, &(sys->opts_ttl), sizeof(sys->opts_ttl)) < 0) {
		printf("ping: unable to set IP_TTL options\n");
		goto error;
	}

	// resolve address
	ret = resolve_addr(sys, sys->addr);
	if (!ret) {
		printf("ping: %s: Name or service not known\n", sys->addr);
		goto error;
	}

	sys->stats_packet_time = 0;
	sys->stats_start_time = 0;
	sys->stats_sent_count = 0;
	sys->stats_recv_count = 0;
	sys->stats_err_count = 0;
	sys->stats_sent_bytes = 0;

	sys->stats_rtt = 0;
	sys->stats_total_rtt = 0;
	sys->stats_rtt_pow2total = 0;
	sys->stats_rtt_min = 0;
	sys->stats_rtt_max = 0;
	sys->stats_rtt_avg = 0;
	sys->stats_rtt_mdev = 0;
	sys->stats_rtt_ewma = 0;
	
	return (1);
error:
	if (sys->ip_addr)
		free((void *)(sys->ip_addr));
	free(sys);
	return (0);
}

void handle_sigint(int num) {
	(void)num;
	long avg_pow2total = 0;
	// update standard deviation
	if (sys->stats_recv_count > 0)
		avg_pow2total = sys->stats_rtt_pow2total / sys->stats_recv_count;
	sys->stats_rtt_mdev = ft_sqrt(avg_pow2total -
									  sys->stats_rtt_avg * sys->stats_rtt_avg);
	print_statistics();
	if (sys->ip_addr)
		free((void *)(sys->ip_addr));
	free(sys);
	exit(0);
}

void handle_sigalarm(int num) {
	(void)num;
	struct timeval time;
	gettimeofday(&time, NULL);
	sys->stats_packet_time = time.tv_sec * 1000000 + time.tv_usec;
	sys->stats_sent_bytes = send_echo_packet(sys, sys->sockfd, sys->opts_packet_size, sys->stats_sent_count + 1);
	sys->stats_sent_count += 1;
	alarm(1);
}

void handle_sigquit(int num) {
	(void)num;
	printf("%d/%d packets, %d%% loss",
		   sys->stats_recv_count, sys->stats_sent_count,
		   (1 - (sys->stats_recv_count / sys->stats_sent_count)) * 100);
	if (sys->stats_recv_count >= 1)
	{
		printf(", min/avg/ewma/max = %ld.%03ld/%ld.%03ld/%ld.%03ld/%ld.%03ld ms",
			   sys->stats_rtt_min/1000l,sys->stats_rtt_min%1000l,
			   sys->stats_rtt_avg/1000l,sys->stats_rtt_avg%1000l,
			   sys->stats_rtt_ewma/1000l,sys->stats_rtt_ewma%1000l,
			   sys->stats_rtt_max/1000l,sys->stats_rtt_max%1000l
			);
	}
	printf("\n");
}

int main(int ac, char **av)
{
	uid_t uid = getuid();
	if (uid != 0) {
		printf("ping: uid not root, need root to use raw sockets \n");
		return (1);
	}
	int ret = init(ac, av);
	if (!ret)
		return (1);
	// free and leave
	signal(SIGINT, &handle_sigint);
	// send icmp packet each `alarm`
	signal(SIGALRM, &handle_sigalarm);
	// print packet stats
	signal(SIGQUIT, &handle_sigquit);

	struct timeval time;
	gettimeofday(&time, NULL);
	size_t total_size = sys->opts_packet_size + sizeof(struct icmphdr) + sizeof(struct iphdr);
	sys->stats_start_time = time.tv_sec * 1000000 + time.tv_usec;
	sys->stats_packet_time = sys->stats_start_time;
	printf("PING %s (%s) %d(%lu) bytes of data\n",
		   sys->addr, sys->ip_addr, sys->opts_packet_size, total_size);
	handle_sigalarm(0);
	while (42) {
		receive_icmp_packet(sys, sys->sockfd, total_size - sizeof(struct iphdr));
		update_statistics();
		// TODO: if (sys->stats_recv_count == sys->opts_packetnum)
		//   signal(SIGALRM, NULL);
		//   break;
	}
	// print_statistics;
	return (0);
}
