#include <stddef.h>
#include <unistd.h>
	
typedef struct s_sys
{
	const char *addr;
	const char *ip_addr;
	struct addrinfo *addr_info;
	struct sockaddr *dest_addr;
	int sockfd;

	int opts_ttl;
	int opts_packet_count;
	int opts_packet_size;
	unsigned char opts_verbose;

	long stats_packet_time;
	long stats_start_time;
	int stats_sent_count;
	int stats_recv_count;
	int stats_err_count;
	ssize_t stats_sent_bytes;

	long stats_rtt;
	long stats_total_rtt;
	long stats_rtt_pow2total;
	long stats_rtt_min;
	long stats_rtt_max;
	long stats_rtt_avg;
	long stats_rtt_mdev;
	long stats_rtt_ewma;
} t_sys;

int set_options(t_sys *sys, int ac, char **av);
int resolve_addr(t_sys *sys, const char *addr);
ssize_t send_echo_packet(t_sys *sys, int sockfd, size_t size, int seq);
void receive_icmp_packet(t_sys *sys, int sockfd, ssize_t packlen);
double ft_sqrt(double n);
void print_statistics(void);
void update_statistics(void);
